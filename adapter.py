class MappingAdapter:
    def __init__(self, adaptee):
        self.adaptee = adaptee

    def lighten(self, grid):
        dim = [len(grid[0]), len(grid)]
        self.adaptee.set_dim(dim)

        lights = []
        for x in range(len(grid)):
            for y in range(len(grid[0])):
                if grid[x][y] == 1:
                    lights.append((y, x))
                    
        obstacles = []
        for x in range(len(grid)):
            for y in range(len(grid[0])):
                if grid[x][y] == -1:
                    obstacles.append((y, x))
   
        self.adaptee.set_lights(lights)
        self.adaptee.set_obstacles(obstacles)
        return self.adaptee.generate_lights()
